#include <iostream>
#include "cpp-subprocess/include/subprocess.hpp"
#include <functional>

void getLines(const std::string& cmd, std::vector<std::string> argv, std::function<void(std::string)> accept)
{
    subprocess::popen prog(cmd, argv);
    std::string line;
    while (std::getline(prog.stdout(), line))
        accept(line);
}
