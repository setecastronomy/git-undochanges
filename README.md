# Tool for controlled reversion of modified git clone directory.

#License
MIT

# Included components

https://github.com/tsaarni/cpp-subprocess

# TODO

Maybe replace cpp-subprocess. It has its advantages (save argument passing, low
overhead), however it requires POSIX and a GNU STL extension, also the read
buffer is apparently very small, causing many syscalls.
