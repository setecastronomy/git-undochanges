#include <iostream>
#include <functional>
#include <vector>

#include <cstdio>
#include <cstdlib>
#include <cstring>

void getLines(const std::string& cmd, std::vector<std::string> argv, std::function<void(std::string)> accept)
{
    char buf[8192];
    buf[8191] = 0;

    auto fullCmd = cmd;
    for(auto& el: argv)
    {
        fullCmd += " ";
        fullCmd += el;
    }
    auto fh = _popen(fullCmd.c_str(), "rt");
    if (!fh)
        throw std::invalid_argument(fullCmd);

    while (fgets(buf, sizeof(buf)-1, fh))
    {
        auto eol = strchr(buf, '\n');
        for(; eol > buf && !isprint((unsigned)*eol); eol--)
            accept(std::string(buf, eol-buf));
    }
}
