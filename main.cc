#include <iostream>
#include <fstream>
#include <string>
#include <string_view>
#include <deque>
#include <set>
#include <functional>
#if defined(_WIN32)
#include "subproc_win32.h"
#else
#include "subproc_posix.h"
#endif

using namespace std;

#define startsWith(where, what) (0==(where).compare(0, (what).size(), (what)))

void getLines(string file, function<void(string&)> accept)
{
    fstream reader;
    reader.open(file, ios::in);
    if (!reader.is_open())
        throw std::invalid_argument(file);
    string line;
    while(getline(reader, line))
        accept(line);
    if (reader.bad())
        throw std::invalid_argument(file);
}

void usage(int code = 1, const char *szErrorHint = nullptr)
{
    if (szErrorHint)
        (code == 0 ? cout : cerr) << "ERROR: " << szErrorHint << endl;
    (code == 0 ? cout : cerr)
            << "USAGE: git-undochanges PATTERN PATTERN ... -i PATTERN-LIST-FILE [ -f ]" << endl
            << "-i\tList of patterns" << endl
            << "-f\tUndo changes immediately" << endl;
    exit(code);
}

bool debug(false), undo(false);

int main(int argc, char** argv)
{
    try
    {
        fstream nix;
        nix.open(".git/config", ios::in);
    }
    catch (...)
    {
        usage(2, "Cannot find .git/config, not starting in git clone directory?");
    }
    vector<string> patargs {"diff"};
    for (int i = 1; i < argc; ++i)
    {
        string arg(argv[i]);

        if (arg == "-d")
        {
            debug = true;
        }
        else if (arg == "-f")
        {
            undo = true;
        }
        else if (arg == "-i")
        {
            ++i;
            if (i == argc)
                usage(1);
            getLines(argv[i], [&](auto& line)
            {
                patargs.emplace_back("-I");
                patargs.emplace_back(move(line));
            });
        }
        else
        {
            patargs.emplace_back("-I");
            patargs.emplace_back(move(arg));
        }
    }
    if (patargs.size() == 1)
    {
        usage(3, "No valid ignore patterns specified");
    }
    set<string> changed;
    string line;
    string pfx("+++ b/");
    getLines("git", {"diff"}, [&](auto line)
    {
        if (startsWith(line, pfx))
            changed.emplace(line.substr(pfx.length()));
    });
    if (debug)
    {
        for(auto& s: changed)
            cerr << "DA: " << s <<endl;
    }
    getLines("git", patargs, [&](auto line)
    {
        if (startsWith(line, pfx))
            changed.erase(line.substr(pfx.length()));
    });
    if (!changed.empty())
    {
        cout << "REVERT:" << endl;
        for(auto& s: changed) cout << s << endl;
    }
    vector<string> args {"checkout", "HEAD", "--"};
    for(auto& el: changed)
        args.emplace_back(move(el));

    if (undo)
    {
        // should not print anything, but better don't hide it
        getLines("git", args, [](auto el) { cerr << el << endl;});
    }
    else
    {
        cerr << "EXECUTE (dry-run):" << endl << "git";
        for(auto& el: args) cerr << " " << el;
        cerr << endl;
    }

    return 0;
}

